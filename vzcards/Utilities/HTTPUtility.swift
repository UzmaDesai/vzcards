//
//  HTTPUtility.swift
//  vzcards
//
//  Created by Girish Rathod on 26/08/15.
//  Copyright (c) 2015 Bitjini. All rights reserved.
//

import Foundation

class HTTPUtility : NSObject{
    
    var baseURL = ""
    override init() {
        
    }
    
    func HTTPsendRequest(request: NSMutableURLRequest, callback: (String, String?) -> Void) {
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            (data, response, error) -> Void in
            if (error != nil) {
                callback("", error.localizedDescription)
            } else {
                callback(NSString(data: data, encoding: NSUTF8StringEncoding)! as String, nil)
        }
        }
        task.resume()
    }
    
    func JSONStringify(jsonObj: AnyObject) -> String {
        var e: NSError?
        let jsonData = NSJSONSerialization.dataWithJSONObject(
            jsonObj
            , options: NSJSONWritingOptions(0)
            , error: &e
        )
        
        if e != nil      {
            return ""
        } else {
            return (NSString(data: jsonData!, encoding: NSUTF8StringEncoding) as? String)!
        }
    }
    
    func HTTPPostJSON(url: String, jsonObj: AnyObject, callback: (String, String?) -> Void) {
        var request = NSMutableURLRequest(URL: NSURL(string: baseURL + url)!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*", forHTTPHeaderField: "Accept")
        request.addValue("application/vnd.justask.com; version=1", forHTTPHeaderField: "Accept")
        
        let jsonString = JSONStringify(jsonObj)
        let data: NSData = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!
        
        request.HTTPBody = data
        
        HTTPsendRequest(request, callback: callback)
    }
    
    func HTTPPostJSONString(url: String, jsonObj: String, callback: (String, String?) -> Void) {
        var request = NSMutableURLRequest(URL: NSURL(string: baseURL + url)!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*", forHTTPHeaderField: "Accept")
//        request.addValue("application/vnd.bitjini.com; version=1", forHTTPHeaderField: "Accept")
        
        let jsonString = jsonObj
        let data: NSData = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!
        
        request.HTTPBody = data
        
        HTTPsendRequest(request, callback: callback)
    }
    func HTTPPatchJSON(url: String, jsonObj: AnyObject, callback: (String, String?) -> Void) {
        var request = NSMutableURLRequest(URL: NSURL(string: baseURL + url)!)
        
        request.HTTPMethod = "PATCH"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*", forHTTPHeaderField: "Accept")
//        request.addValue("application/vnd.bitjini.com; version=1", forHTTPHeaderField: "Accept")
        
        let jsonString = JSONStringify(jsonObj)
        let data: NSData = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!
        
        request.HTTPBody = data
        
        HTTPsendRequest(request, callback: callback)
    }
    
    func HTTPGetJSON(url: String, callback: (String, String?) -> Void) {
        var request = NSMutableURLRequest(URL: NSURL(string: baseURL + url)!)
        
        request.HTTPMethod = "GET"
        request.addValue("*/*", forHTTPHeaderField: "Accept")
//        request.addValue("application/vnd.bitjini.com; version=1", forHTTPHeaderField: "Accept")
        HTTPsendRequest(request, callback: callback)
        
    }
    
}